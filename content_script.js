var key = $("#key-val").text().trim();
var title = $("#summary-val").text().trim();
var type = $("#type-val").text().trim();
var reporter = $("#reporter-val").text().trim();
var created_date = new Date($("#create-date time").attr("datetime")).toDateString();


chrome.extension.sendRequest({key: key, title: title, type: type, reporter: reporter, created_date: created_date});
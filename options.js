window.onload = function(){
    getAddress();
};

// Saves address to localStorage and calls for update confirmation and updates displayed address.
function saveAddress() {
    localStorage.serverAddress = document.getElementById("serverAddress").value;
    updateStatus();
    getAddress();
}

// Update status to let user know options were saved.
function updateStatus() {
    var status = document.getElementById("status");
    status.innerHTML = "Address Saved.";
    setTimeout(function() {
        status.innerHTML = "";
    }, 750);
}

// Gets the locally stored address
function getAddress() {
    var data,
        savedDiv = document.getElementById("saved");
    
    data = localStorage.serverAddress;
    if (data) {
        savedDiv.innerHTML = "Current server URL: " + data;
    } else {
        savedDiv.innerHTML = "There is currently no server URL stored";
    }
}

// Load the stored value back into the text field on load
function restoreOptions() {
    var stored = localStorage.serverAddress;
    if (!stored) {
        return;
    }
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.addEventListener('DOMContentLoaded', getAddress);

document.querySelector('#save').addEventListener('click', saveAddress);

chrome.extension.onRequest.addListener(
    function(request, sender, sendResponse) {
        var link = localStorage.serverAddress + "/template/ticket.html?id=";

        link += request.key + "&title=";
        link += request.title + "&created=";
        link += request.created_date +"&reporter=";
        link += request.reporter + "&type=";
        link += request.type;

        $("#ticket").html("<iframe height='600' width='800' src='" + link + "'></iframe>");
});

chrome.tabs.executeScript(null, {file: "jquery-1.6.1.js"});
chrome.tabs.executeScript(null, {file: "content_script.js"});